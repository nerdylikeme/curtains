/// <reference path="../../typings/angular/angular.d.ts" />
/// <reference path="services/dataService.ts" />
/// <reference path="services/storageService.ts" />
/// <reference path="services/authenticationService.ts" />

var app = angular.module('app', ['curtains.catalog']);

app.service('dataService', Services.DataService);
app.service('storageService', Services.StorageService);
app.service('authenticationService', ['dataService', 'storageService', Services.AuthenticationService]);
