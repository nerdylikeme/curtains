﻿
module Controllers.Catalog {
  export enum ViewType { Grid, Card }
  export interface IMediaListController {
    name: string
    view: ViewType
  }
  export class MovieListController implements IMediaListController {
    name: string
    view: ViewType
    /**
     * Creates a new MovieListController instance.
     */
    constructor() {
      this.name = "Movies";
      this.view = ViewType.Grid;
    }
  }

  export class MusicListController implements IMediaListController {
    name: string
    view: ViewType
    /**
     * Creates a new MusicListController instance.
     */
    constructor() {
      this.name = "Music";
      this.view = ViewType.Card;
    }
  }
}
