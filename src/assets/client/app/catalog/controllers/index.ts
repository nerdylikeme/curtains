﻿/// <reference path="../../../../typings/angular/angular.d.ts" />
/// <reference path="catalog.navigation.ts" />
/// <reference path="catalog.list.ts" />

module Controllers.Catalog {
  var catalog = angular.module('module.catalog.controllers', []);

  catalog.controller('navigation.ctrl', NavigationController);
  catalog.controller('movie.list.ctrl', MovieListController);
  catalog.controller('music.list.ctrl', MusicListController);
}