﻿/// <reference path="../../../typings/angular/angular.d.ts" />
/// <reference path="../../../typings/angular/angular-route.d.ts" />

var catalog = angular.module('curtains.catalog', ['module.catalog.controllers', 'ngRoute']);

catalog.config(['$routeProvider', (routeProvider: ng.route.IRouteProvider) => {
  routeProvider
    .when('/movies', {
      controllerAs: 'listctrl',
      controller: 'movie.list.ctrl',
      templateUrl: '/app/catalog/views/list.html'
    })
    .when('/music', {
      controllerAs: 'listctrl',
      controller: 'music.list.ctrl',
      templateUrl: '/app/catalog/views/list.html'
    });
}]);