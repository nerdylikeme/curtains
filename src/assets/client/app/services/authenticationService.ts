/// <reference path="dataService.ts" />
/// <reference path="storageService.ts" />

module Services {
  export interface IAuthenticationService {
    /**
     * Authenticates the user and stores a token in local storage.
     * @param {string} username the username.
     * @param {string} password the password.
     */
    authenticate(username: string, password: string)
  }
  export class AuthenticationService implements IAuthenticationService {
    // repsonsible for authenticating against the server and fetching an authentication token.
    dataService: IDataService;
    // responsible for storing credentials.
    storageService: IStorageService;
    /**
     * Creates a new authentication service instance.
     * @param {IDataService} dataService the data service.
     * @param {IStorageService} storageService the storage service.
     */
    constructor(dataService: IDataService, storageService: IStorageService) {
      this.dataService = dataService;
      this.storageService = storageService;
    }
    /**
     * Authenticates the user and stores a token in local storage.
     * @param {string} username the username.
     * @param {string} password the password.
     */
    authenticate(username: string, password: string) {
    }
  }
}
