module Services {
  export interface IStorageService {
    /**
     * Removes an item from storage.
     * @param {string} key the key to remove.
     */
    removeItem(key: string)
    /**
     * Gets an item from storage.
     * @param {string} key the key of the item to retreive.
     */
  	getItem(key: string)
    /**
     * Sets an item in storage.
     * @param {string} key the key of the item.
     * @param value the value to store.
     */
    setItem(key: string, value)
    /**
     * Determines if the storage service is available.
     * @returns {boolean} True if the storage service is available; Otherwise, false.
     */
    isAvailable(): boolean
  }

  export class StorageService implements IStorageService {
    private available: boolean
    constructor() {
      // determine whether or not html5 storage is supported.
      this.available = this.determineAvailability();
    }
    /**
     * Removes an item from storage.
     * @param {string} key the key to remove.
     */
    removeItem(key: string) {
      localStorage.removeItem(key);
    }
    /**
     * Gets an item from storage.
     * @param {string} key the key of the item to retreive.
     */
    getItem(key: string) {
      return localStorage.getItem(key);
    }
    /**
     * Sets an item in storage.
     * @param {string} key the key of the item.
     * @param value the value to store.
     */
    setItem(key: string, value) {
      localStorage.setItem(key, value);
    }
    isAvailable(): boolean {
      return this.available;
    }

    private determineAvailability() {
      if (typeof (Storage) !== "undefined") {
        console.log("storage service is capable");
        try {
          this.setItem('testing', '1');
          this.removeItem('testing');
          return true;
        } catch (e) {
          console.log("Testing the storage service failed.  This could be due to restrictions on the browser (i.e. private mode).");
        }
      }
      return false;
    }
  }

  export class InMemoryStorageService implements IStorageService {
    map: { [key: string]: any; }
    constructor() {
      this.map = {};
    }
    /**
     * Removes an item from storage.
     * @param {string} key the key to remove.
     */
    removeItem(key: string) {
      delete this.map[key];
    }
    /**
     * Gets an item from storage.
     * @param {string} key the key of the item to retreive.
     */
    getItem(key: string) {
      return this.map[key];
    }
    /**
     * Sets an item in storage.
     * @param {string} key the key of the item.
     * @param value the value to store.
     */
    setItem(key: string, value) {
      this.map[key] = value;
    }
    isAvailable(): boolean {
      return true;
    }
  }

  export class QueryStringStorageService extends InMemoryStorageService {
    /**
     * Removes an item from storage.
     * @param {string} key the key to remove.
     */
    removeItem(key: string) {
      super.removeItem(key);
    }
    /**
     * Gets an item from storage.
     * @param {string} key the key of the item to retreive.
     */
    getItem(key: string) {
      return super.getItem(key);
    }
    /**
     * Sets an item in storage.
     * @param {string} key the key of the item.
     * @param value the value to store.
     */
    setItem(key: string, value) {
      super.setItem(key, value);
    }
    isAvailable(): boolean {
      return super.isAvailable();
    }
  }
}
