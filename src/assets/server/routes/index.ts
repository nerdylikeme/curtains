import site = require('./site/index')

export function configure(app) {
  site.configure(app);
}
