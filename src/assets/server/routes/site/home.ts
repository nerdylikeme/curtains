/**
 * GET: /
 * Gets the home page.
 */
export function index(req, res) {
  res.render('home/index', { title: 'Express' });
}
