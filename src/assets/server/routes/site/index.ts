import home = require('./home')


/**
 * Configures the site routes.
 */
export function configure(app) {
  app.get('/', home.index);
}
