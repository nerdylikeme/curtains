/// <reference path="../typings/node/node.d.ts" />

import routes = require('./routes/index');
import http = require('http');
import path = require('path');

var express = require('express');

/**
 * Runs this server.
 */
export function run(port: number) {
  var app = express();

  // all environments
  app.set('port', port);
  app.set('views', path.join(process.cwd(), 'views'));
  app.set('view engine', 'jade');
  app.use(express.static(path.join(__dirname, 'public')));

  // development only
  if ('development' == app.get('env')) {
    //app.use(express.errorHandler());
  }

  routes.configure(app);

  http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
  });
}
