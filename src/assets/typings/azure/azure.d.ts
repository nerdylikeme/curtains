declare module "azure" {
    export function createTableService(storageAccountName?: string, storageAccessKey?: string): TableService;
    export interface TableService {
        createTableIfNotExists(tableName: string, callback: (error: Error) => void);
    }
}