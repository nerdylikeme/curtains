var gulp = require('gulp')
  , jade = require('gulp-jade')
  , clean = require('gulp-clean')
  , typescript = require('gulp-tsc');

gulp.task('typescript-compile-server', function () {
  return gulp.src(['*.ts'], { cwd: 'assets/server/**' })
    .pipe(typescript({ module: 'commonjs' }))
    .pipe(gulp.dest(''));
});

gulp.task('copy-typescript-client', function () {
  return gulp.src(['*.ts'], { cwd: 'assets/client/app/**' })
    .pipe(gulp.dest('public/app'));
});

gulp.task('typescript-compile-catalog', function () {
  return gulp.src(['*.ts'], { cwd: 'assets/client/app/catalog/**' })
    .pipe(typescript({ sourcemap: true, target: 'ES5', sourceRoot: '/app/catalog', out: 'catalog.js' }))
    .pipe(gulp.dest('public/app/catalog'));
});

gulp.task('typescript-compile-app', function () {
  return gulp.src(['*.ts'], { cwd: 'assets/client/app' })
    .pipe(typescript({ sourcemap: true, target: 'ES5', sourceRoot: '/app/', out: 'app.js' }))
    .pipe(gulp.dest('public/app'));
});

gulp.task('jade', function () {
  return gulp.src(['*.jade'], { cwd: 'assets/client/app/**' })
    .pipe(jade())
    .pipe(gulp.dest('public/app'));
});

gulp.task('clean-client', function () {
  return gulp.src(['public/app/'])
    .pipe(clean());
});

gulp.task('build', ['build-client', 'build-server']);
gulp.task('build-server', ['typescript-compile-server']);
gulp.task('build-client', ['typescript-compile-client', 'jade']);
gulp.task('typescript-compile-client', ['typescript-compile-catalog', 'typescript-compile-app', 'copy-typescript-client']);

gulp.task('client-dev', ['build-client'], function () {
  gulp.watch('assets/client/**/*.ts', ['typescript-compile-client']);
  gulp.watch('assets/client/**/*.jade', ['jade']);
});
